var eventoClick = (navigator.userAgent.match(/iPad/i)) ? "touchstart" : "click";

$(function () {
	navegacao();
	metodosJqueryValidateAdicionais();
	validacoes();
	formModal();
	busca();
	preencheDadosAlunos();
});

function navegacao() {
	$('.jq-controle-navegacao').find('.nav-item').on(eventoClick, function(){
		var linkClicado = $(this).find('a');
		var valorClicado = linkClicado.data('mudanca');
		$('.nav-item').find('a').removeClass('active');
		linkClicado.addClass('active');
		$('.card-body').hide();
		$('.card-body[data-conteudo="'+valorClicado+'"]').show();
	});
}

function metodosJqueryValidateAdicionais() {
	jQuery.validator.addMethod("lettersonly", function(value, element) {
	  return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
	}, ''); 

	jQuery.validator.addMethod("cpf", function(value, element) {
  	 	value = jQuery.trim(value);

    	value = value.replace('.','');
    	value = value.replace('.','');
    	cpf = value.replace('-','');
    	while(cpf.length < 11) cpf = "0"+ cpf;
    	var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    	var a = [];
    	var b = new Number;
    	var c = 11;
    	for (i=0; i<11; i++){
    	    a[i] = cpf.charAt(i);
    	    if (i < 9) b += (a[i] * --c);
    	}
    	if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
    	b = 0;
    	c = 11;
    	for (y=0; y<10; y++) b += (a[y] * c--);
    	if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

    	var retorno = true;
    	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;

    	return this.optional(element) || retorno;

	}, '');
}

function validacoes() {
	$("#insere-aluno").validate({
	    rules : {
	        nome:{
	           required:true,
	           lettersonly:true
	        },
	        cpf:{
	           required:true,
	           cpf:true
	        },
	        matricula:{
	           required:true,
	           number:true
	        },
	        nota: {
	        	required:true,
	        	range:[0.00,10.00] 
	        },
	        rua: {
	        	required:true,
	        	lettersonly:true
	        },
	        numero: {
	        	required:true,
	        	number:true
	        },
	        bairro: {
	        	required:true,
	        	lettersonly:true
	        }
	    },
	    messages:{
	         nome: {
	            required:"Informe o nome do aluno",
	            lettersonly:"O nome só aceita letras e espaços"
	         },
	         cpf: {
	            required:"Insira o cpf",
	            cpf:"Informe um cpf válido"
	         },
	         matricula: {
	            required:"Insira a matrícula",
	            number: "Insira apenas números na matrícula"
	         },
	         nota: {
	         	required: "Insira a nota",
	         	range: "Insira uma nota entre 0.00 e 10.00"
	         },
	         rua: {
	         	required: "Insira a rua",
	         	lettersonly:"A rua não pode ter números"
	         },
	         numero: {
	         	required: "Insira o número",
	         	number: "Insira apenas números"
	         },
	         bairro: {
	         	required: "Insira o bairro",
	         	lettersonly:"O bairro não pode ter números"
	         }
	    }
	});
	$('#form-nota-adicional').validate({
		rules : {
	        notaAdicional:{
	           	required:true,
	        	range:[0.00,10.00] 
	        }
	    },
	    messages:{
	        notaAdicional: {
	            required: "Insira a nota",
	         	range: "Insira uma nota entre 0.00 e 10.00"
	        }
	    },
	    validClass: "success"
	});
}

function formModal() {
	$('#form-nota-adicional').on('submit', function(e) {
		e.preventDefault();

		if($('input[name="nota[]"').val().length == 0) alert('Preencha a primeira nota antes de adicionar outras');
		else {
			if($(this).find('input[name="notaAdicional"]').hasClass('success')) {
				var valorNota = $(this).find('input[name="notaAdicional"]').val();
				var htmlNovoInput = '<input type="text" class="form-control" name="nota[]" value="'+valorNota+'" />';
				$('.jq-grupo-notas').find('.jq-caixa-notas').append(htmlNovoInput);
				$(this).find('input[name="notaAdicional"]').removeClass('success');
				$('#modal-form').modal('hide');
			}
		}
		
		return false;
	});
}

function busca() {
	$('#busca-aluno').on('submit', function(e) {

		e.preventDefault();

		var valor 	  = $(this).find('input[name="valor"]').val();
		var tipoBusca = $(this).find('select[name="tipoBusca"]').val();

		switch(tipoBusca) {
			case '0': //cpf
				if(!validaCPF(valor)) {
					alert('Insira um cpf válido.');
					return false;
				}
				break;
			case '1': //nome
				if(!validaApenasLetras(valor)) {
					alert('O nome só aceita letras e espaço.');
					return false;
				}
				break;
			case '2': //matricula
				if(!validaApenasNumeros(valor)) {
					alert('Insira apenas números.');
					return false;
				}
				break;
		}

		$.ajax({
			url: 'buscaAluno',
			type: 'POST',
			 headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		},
    		data: {
    			'valor' : valor,
    			'tipoBusca' : tipoBusca
    		},
    		dataType: 'json'
    		, beforeSend: function()
    		{
    			$('.jq-resultado-busca').hide();
    		}
    		, success: function(retorno)
			{
				if(!retorno.resultado) alert('Nenhum resultado encontrado.');
				else preencheResultadoBusca(retorno.resultado);
			}
			, error : function (error) 
			{
            	console.log(error);
        	}

		});
		return false;
	});
}

function preencheResultadoBusca(conteudo) {
	$('.jq-resultado-busca').show();
	html =  '<form class="col-sm-12" id="editar-aluno" data-id="'+conteudo.usuario_id+'" data-endereco-id="'+conteudo.endereco_id+'">';

	html += '<div class="form-group row">';
	html += '	<label class="text-left col-sm-2">Nome</label>';
	html += ' 	<div class="col-sm-10"><input type="text" class="form-control" name="nome" value="'+conteudo.nome+'" /></div>';
	html += '</div>';
	html += '<div class="form-group row">';
	html += '	<label class="text-left col-sm-2">CPF</label>';
	html += '	<div class="col-sm-10"><input type="text" class="form-control" name="cpf" value="'+conteudo.cpf+'" /></div>';
	html += '</div>';
	html += '<div class="form-group row">';
	html += '	<label class="text-left col-sm-2">Matrícula</label>';
	html += '	<div class="col-sm-10"><input type="text" class="form-control" name="matricula" value="'+conteudo.matricula+'" /></div>';
	html += '</div>';
	html += '<div class="form-group row">';
	html += '	<label class="text-left col-sm-2">Nota(média)</label>';
	html += '	<div class="col-sm-10"><input disabled type="text" class="form-control" name="media" value="'+conteudo.media+'" /></div>';
	html += '</div>';
	html += '<div class="form-group row">';
	html += '	<label class="text-left col-sm-2">Rua</label>';
	html += '	<div class="col-sm-10"><input type="text" class="form-control" name="rua" value="'+conteudo.logradouro+'" /></div>';
	html += '</div>';
	html += '<div class="form-group row">';
	html += '	<label class="text-left col-sm-2">Número</label>';
	html += '	<div class="col-sm-10"><input type="text" class="form-control" name="numero" value="'+conteudo.numero+'" /></div>';
	html += '</div>';
	html += '<div class="form-group row">';
	html += '	<label class="text-left col-sm-2">Bairro</label>';
	html += '	<div class="col-sm-10"><input type="text" class="form-control" name="bairro" value="'+conteudo.bairro+'" /></div>';
	html += '</div>';
	
	html += '	<input type="submit" class="btn btn-large btn-outline-primary pointer" value="Editar" />';
	html += '	<button type="button" class="btn btn-large btn-outline-danger pointer jq-excluir-aluno" >Excluir</button>';
	html += '</form>';
	$('.jq-resultado-busca').html(html);
	editarAluno();
	excluirAluno();
}

function editarAluno() {
	$('#editar-aluno').on('submit', function(e) {

		e.preventDefault();

		var nome 		= $(this).find('input[name="nome"]').val();
		var cpf 		= $(this).find('input[name="cpf"]').val();
		var matricula 	= $(this).find('input[name="matricula"]').val();
		var rua 		= $(this).find('input[name="rua"]').val();
		var numero 		= $(this).find('input[name="numero"]').val();
		var bairro 		= $(this).find('input[name="bairro"]').val();
		var usuarioId   = $(this).data('id');
		var enderecoId  = $(this).data('endereco-id');

		$.ajax({
			url: 'editaAluno',
			type: 'POST',
			 headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		},
    		data: {
    			'nome' : nome,
    			'cpf' : cpf,
    			'matricula' : matricula,
    			'rua' : rua,
    			'numero' : numero,
    			'bairro' : bairro,
    			'usuario_id' : usuarioId,
    			'endereco_id': enderecoId
    		},
    		dataType: 'json'
    		, success: function(retorno)
			{
			    
			    if(retorno.success) alert('Cadastro alterado com sucesso.');
				else alert(retorno.msgErro);
			}
			, error : function (error) 
			{
            	alert('Ocorreu um erro na edição, tente novamente mais tarde');
        	}

		});
		return false;
	});
}

function excluirAluno() {
	//TODO "realmente deseja excluir?"
	$('.jq-excluir-aluno').on(eventoClick, function(e){
		e.preventDefault();
		var self = $(this);
		$.confirm({
		    title: 'Deseja realmente excluir?',
		    content: '',
		    buttons: {
		        confirm: function () {
		            var usuarioId = self.closest('form').data('id');
		            $.ajax({
						url: 'excluiAluno',
						type: 'POST',
						 headers: {
        					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    					},
    					data: {
    						'usuario_id' : usuarioId
    					},
    					dataType: 'json'
    					, success: function(retorno)
						{
						    alert('Aluno removido com sucesso.');
						    $('.jq-resultado-busca').html('');
						}
						, error : function (error) 
						{
            				alert('Ocorreu um erro na exclusão, tente novamente mais tarde');
        				}
					});
		        },
		        cancel: function () {}
		    }
		});
		return false;
	});
}

function preencheDadosAlunos() {
	$.ajax({
		url: 'rankAluno',
		type: 'POST',
		 headers: {
       		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	},
    	dataType: 'json'
    	, success: function(retorno) {
    		console.log(retorno);
    		if(retorno.mediaTurma) $('.jq-media-turma').html(retorno.mediaTurma);
    		else $('.jq-media-turma').html('Sem registro');
    		if(retorno.melhorAluno) $('.jq-melhor-aluno').html(retorno.melhorAluno);
    		else $('.jq-media-turma').html('Sem registro');
    		if(retorno.piorAluno) $('.jq-pior-aluno').html(retorno.piorAluno);
    		else $('.jq-media-turma').html('Sem registro');

    		console.log(retorno.grupoAlunos.length)

    		if(retorno.grupoAlunos.length != 0) {
    			var htmlRank = '';
    			htmlRank += '<div class="col-sm-6 text-left fundo-cinza borda altura-max"><p><strong>Nome</strong></p></div>';
    			htmlRank += '<div class="col-sm-2 text-left fundo-cinza borda altura-max"><p><strong>Nota</strong></p></div>';
    			htmlRank += '<div class="col-sm-4"></div>';
    			for(var i=0; i< retorno.grupoAlunos.length; i++) {
    				i%2==0 ? zebra = 'zebrado' : zebra = '';
    				htmlRank += '<div class="col-sm-6 text-left '+zebra+' borda altura-max"><p>'+retorno.grupoAlunos[i].nome+'</p></div>';
    				htmlRank += '<div class="col-sm-2 text-left '+zebra+' borda altura-max"><p>'+retorno.grupoAlunos[i].nota+'</p></div>' 
    				htmlRank += '<div class="col-sm-4"></div>';
    			}
    			$('.jq-rank').html(htmlRank);
    		} else {
    			var htmlRank = '<div class="col-sm-6 text-left"><p>Sem alunos registrados.</p></div>';
    			htmlRank += '<div class="col-sm-6 text-left"></div>';
    			$('.jq-rank').html(htmlRank);
    		}
    	}
		, error : function (error) {}
	});
}

function validaCPF(cpf) {
	cpf = cpf.replace(/[^\d]+/g,'');
	if(cpf == '') return false;
	// Elimina CPFs invalidos conhecidos
	if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")       return false;
	// Valida 1o digito
	add = 0;    for (i=0; i < 9; i ++)       add += parseInt(cpf.charAt(i)) * (10 - i);  rev = 11 - (add % 11);  if (rev == 10 || rev == 11)     rev = 0;    if (rev != parseInt(cpf.charAt(9)))     return false;
	// Valida 2o digito
	add = 0;    for (i = 0; i < 10; i ++)        add += parseInt(cpf.charAt(i)) * (11 - i);  rev = 11 - (add % 11);  if (rev == 10 || rev == 11)     rev = 0;    if (rev != parseInt(cpf.charAt(10)))        return false;           return true;
}

function validaApenasLetras(valor) {
	var patt = new RegExp("^[a-zA-Z ]+$");
	var res = patt.test(valor);
	return res;
}

function validaApenasNumeros(valor) {
	var patt = new RegExp('^[0-9]+$');
	var res = patt.test(valor);
	return res;
}