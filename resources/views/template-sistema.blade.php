<html>
    <head>
        <title>@yield('title')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="js/jquery-3.2.1.min.js" type="application/javascript"></script>
        <script src="framework-front/tether.min.js"></script>
        <link href="framework-front/bootstrap-v4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet" />
        <link href="framework-front/bootstrap-v4.0.0-alpha.6/css/bootstrap-grid.min.css" rel="stylesheet" />
        <link href="framework-front/bootstrap-v4.0.0-alpha.6/css/bootstrap-reboot.min.css" rel="stylesheet" />
        <link href="css/home.css" rel="stylesheet" />
        <link href="framework-front/jq-confirm.min.css" rel="stylesheet" />
        <script src="framework-front/bootstrap-v4.0.0-alpha.6/js/bootstrap.min.js" type="application/javascript"></script>
        <script src="js/jquery.validate.min.js" type="application/javascript"></script>
        <script src="framework-front/jq-confirm.min.js" type="application/javascript"></script>
        <script src="js/home.js" type="application/javascript"></script>        
    </head>
    <body>
      <div class="container">
        <div class="card text-center">
  			  <div class="card-header">
  			    <ul class="nav nav-pills card-header-pills jq-controle-navegacao">
  			      <li class="nav-item">
  			        <a class="nav-link active" data-mudanca="cadastro" href="#">Cadastrar</a>
  			      </li>
  			      <li class="nav-item">
  			        <a class="nav-link" data-mudanca="busca" href="#">Busca de Aluno</a>
  			      </li>
  			      <li class="nav-item">
  			        <a class="nav-link" data-mudanca="notas" href="#">Notas</a>
  			      </li>
  			    </ul>
  			  </div>

  			  <div class="card-body espacamento-vertical-1 row" data-conteudo="cadastro">
            <div class="col-sm-3 hidden-xs"></div>
            <div class="col-sm-6 col-xs-12">
              {{ Form::open(array('action' => 'HomeController@insereAluno', 'id' => 'insere-aluno')) }}

                <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                {{ csrf_field() }}

                <div class="form-group row">
                  {{ Form::label('Nome', '', array('class' => 'text-left col-sm-2')) }}
                  <div class="col-sm-10">{{ Form::text('nome', @$nome, array('class' => 'form-control')) }}</div>
                </div>

                <div class="form-group row">
                  {{ Form::label('CPF', '', array('class' => 'text-left col-sm-2')) }}
                  <div class="col-sm-10">{{ Form::text('cpf', @$cpf, array('class' => 'form-control')) }}</div>
                </div>

                <div class="form-group row">
                  {{ Form::label('Matrícula', '', array('class' => 'text-left col-sm-2')) }}
                  <div class="col-sm-10">{{ Form::text('matricula', @$matricula, array('class' => 'form-control')) }}</div>
                </div>

                <div class="form-group row jq-grupo-notas">
                  {{ Form::label('Nota', '', array('class' => 'text-left col-sm-2')) }}
                  <div class="col-sm-8 jq-caixa-notas">{{ Form::text('nota[]', @$nota, array('class' => 'form-control')) }}</div>
                  <div class="col-sm-2"><button type="button" data-toggle="modal" class="btn btn-large btn-primary pointer" data-target="#modal-form">+</button></div>
                </div>

                <div class="form-group row">
                  {{ Form::label('Rua', '', array('class' => 'text-left col-sm-2')) }}
                  <div class="col-sm-10">{{ Form::text('rua', @$rua, array('class' => 'form-control')) }}</div>
                </div>

                <div class="form-group row">
                  {{ Form::label('Número', '', array('class' => 'text-left col-sm-2')) }}
                  <div class="col-sm-10">{{ Form::text('numero', @$numero, array('class' => 'form-control')) }}</div>
                </div>

                <div class="form-group row">
                  {{ Form::label('Bairro', '', array('class' => 'text-left col-sm-2')) }}
                  <div class="col-sm-10">{{ Form::text('bairro', @$bairro, array('class' => 'form-control')) }}</div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">{{ Form::submit('Salvar', ['class' => 'btn btn-large btn-primary pointer']) }}</div>
                  <div class="col-sm-2"></div>
                </div>
          
              {{ Form::close() }}
            </div>
            <div class="col-sm-3 hidde-xs"></div>
  			  </div>

          <div class="card-body espacamento-vertical-1 none" data-conteudo="busca">
              {{ Form::open(array('action' => 'HomeController@buscaAluno', 'id' => 'busca-aluno', 'class'=>'col-sm-12')) }}

                {{ csrf_field() }}

                <div class="row">
                  <div class="col-sm-8">{{ Form::text('valor', @$valor, array('class' => 'inline form-control')) }}</div>
                  <div class="col-sm-2">{{ Form::select('tipoBusca', ['CPF', 'Nome', 'Matrícula'], null, array('class' => 'form-control')) }}</div>
                  <div class="col-sm-2">{{ Form::submit('Buscar', ['class' => 'inline btn btn-large btn-primary pointer']) }}</div>
                </div>
          
              {{ Form::close() }}

              <div class="resultado-busca jq-resultado-busca none">
                
              </div>
          </div>

          <div class="card-body espacamento-vertical-1 none" data-conteudo="notas">
            <div class="row">

              <div class="col-sm-2"></div>
              <div class="col-sm-8">
                <p class="text-left"><strong>Média da turma</strong></p>
                <p class="jq-media-turma text-left"></p>
              </div>
              <div class="col-sm-2"></div>

              <div class="col-sm-2"></div>
              <div class="col-sm-4">
                <p class="text-left"><strong>Melhor aluno</strong></p>
                <p class="jq-melhor-aluno text-left"></p>
              </div> 
              <div class="col-sm-4">
                <p class="text-left"><strong>Pior aluno</strong></p>
                <p class="jq-pior-aluno text-left"></p>
              </div>
              <div class="col-sm-2"></div>

              <div class="col-sm-2"></div>
              <div class="col-sm-10"><p class="text-left"><strong>Alunos:</strong></p></div>
              <div class="col-sm-2"></div>

            </div>

            <div class="row">
              <div class="col-sm-2"></div>
              <div class="row col-sm-10 jq-rank"></div>
              <div class="col-sm-2"></div>
            </div>
          </div>
			  </div>
      </div>
      <div class="modal fade" id="modal-form" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <div class="col-sm-12 text-center"><p class="modal-title">Nota</p></div>
            </div>
            <div class="modal-body padding-zero">
              <form id="form-nota-adicional" class="margin-zero">
                <div class="col-sm-12 text-center espacamento-vertical-2"><input type="text" name="notaAdicional" class="input-menor" /></div>
                <div class="col-sm-12 text-center espacamento-vertical-2 padding-zero"><input type="submit" class="btn btn-large btn-primary pointer btn-esticado" value="OK" /></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </body>
</html>