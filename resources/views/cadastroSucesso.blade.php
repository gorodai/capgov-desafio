<!DOCTYPE>
<html>
	<head>
		<title>@yield('title')</title>
        <link href="framework-front/bootstrap-v4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet" />
        <link href="framework-front/bootstrap-v4.0.0-alpha.6/css/bootstrap-grid.min.css" rel="stylesheet" />
        <link href="framework-front/bootstrap-v4.0.0-alpha.6/css/bootstrap-reboot.min.css" rel="stylesheet" /> 
	</head>
	<body>
		<div class="container">
			<div class="alert alert-success">
  				<p>Cadastro feito com sucesso!</p>
			</div>
			<a href="./home">voltar</a>
		</div>
	</body>
</html>
