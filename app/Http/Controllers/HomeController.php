<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Input;
use App\Aluno;
use App\Endereco;
use App\Nota;

class HomeController extends Controller
{
	public function insereAluno(Request $request)
    {

        $validaNome                 = HomeController::validaApenasLetras($request->nome);
        $validaCPF                  = HomeController::validaCpf($request->cpf);
        $verificaCPFRepetido        = HomeController::verificaCampoRepetido('cpf', $request->cpf);
        $verificaMatriculaRepetida  = HomeController::verificaCampoRepetido('matricula', $request->matricula);
        foreach($request->nota as $nota) {
            $validaNota             = HomeController::validaNota($nota);
            if(!$validaNota) break;
        }
        $validaRua                  = HomeController::validaApenasLetras($request->rua);
        $validaNumero               = HomeController::validaApenasNumero($request->numero);
        $validaBairro               = HomeController::validaApenasLetras($request->bairro);

        $erro = '';
        if(!$validaNome)                $erro .= 'O nome deve ter apenas letras e espaços. ';
        if(!$validaCPF)                 $erro .= 'CPF inválido. ';
        if($verificaCPFRepetido)        $erro .= 'Esse cpf já está registrado. ';
        if($verificaMatriculaRepetida)  $erro .= 'Essa matrícula já foi inserida. ';
        if(!$validaNota)                $erro .= 'Nota inválida, entre com um número entre 0 e 10. ';
        if(!$validaRua)                 $erro .= 'A rua deve ter apenas letras e espaços. ';
        if(!$validaNumero)              $erro .= 'O número deve ter somente caracteres numéricos. ';
        if(!$validaBairro)              $erro .= 'O bairro deve ter apenas letras e espaços. ';

        if(!$validaNome || !$validaCPF || $verificaCPFRepetido || $verificaMatriculaRepetida || !$validaNota || !$validaRua || !$validaNumero || !$validaBairro) return view('erro', array('msgErro' => $erro));

        $ultimo_id              = Endereco::orderBy('id', 'DESC')->first();

        if(!$ultimo_id) $ultimo_id = 0;
        else $ultimo_id = $ultimo_id->id;

        $endereco               = new Endereco;
        $endereco->id           = $ultimo_id + 1;
        $endereco->logradouro   = $request->rua;
        $endereco->numero       = $request->numero;
        $endereco->bairro       = $request->bairro;
        
        $enderecoSalvo          = $endereco->save();

        if($enderecoSalvo) {
            $ultimo_id          = Aluno::orderBy('id', 'DESC')->first();
            if(!$ultimo_id) $ultimo_id = 0;
            else $ultimo_id = $ultimo_id->id;
            $aluno              = new Aluno;
            $aluno->id          = $ultimo_id+1;
            $aluno->nome        = $request->nome;
            $aluno->cpf         = $request->cpf;
            $aluno->matricula   = $request->matricula;
            $aluno->endereco_id = $endereco->id;
            $alunoSalvo = $aluno->save();
            if($alunoSalvo) {
                foreach($request->nota as $nota) {
                     $ultimo_id    = Nota::orderBy('id', 'DESC')->first();
                     if(!$ultimo_id) $ultimo_id = 0;
                    else $ultimo_id = $ultimo_id->id;
                     $novaNota         = new Nota;
                     $novaNota->id     = $ultimo_id+1;
                     $novaNota->valor   = (float)$nota;
                     $novaNota->aluno_id = $aluno->id;
                     $novaNota->save();
                 }
            }
        }

        return view('cadastroSucesso');
    }

    public function buscaAluno(Request $request) 
    {
        switch ($request->tipoBusca) 
        {
            case '0': //cpf
                $resultado = Aluno::where('cpf', $request->valor);
                break;
            case '1': //nome
                $resultado = Aluno::where('nome', 'like', '%'. $request->valor .'%');
                break;
            case '2': //matrícula
                $resultado = Aluno::where('matricula', $request->valor);
                break;
        }
        if(!$resultado->first()) {
            return Response::json(array(
                'success' => true,
                'resultado' => null
            )); 
        }
        $usuarioId  = $resultado->first()->id;
        $resultado  = $resultado->join('endereco', 'endereco.id', '=', 'aluno.endereco_id')->first();
        $media      = Nota::where('aluno_id', $usuarioId)->avg('valor');
        $resultado->media = $media;
        $resultado->usuario_id = $usuarioId;

        return Response::json(array(
            'success' => true,
            'resultado'   => $resultado
        )); 
    }

    public function editaAluno(Request $request) 
    {
        $validaNome                 = HomeController::validaApenasLetras($request->nome);
        $validaCPF                  = HomeController::validaCpf($request->cpf);
        $verificaCPFRepetido        = HomeController::verificaEdicaoCampoRepetido('cpf', $request->cpf, $request->usuario_id);
        $verificaMatriculaRepetida  = HomeController::verificaEdicaoCampoRepetido('matricula', $request->matricula, $request->usuario_id);
        $validaRua                  = HomeController::validaApenasLetras($request->rua);
        $validaNumero               = HomeController::validaApenasNumero($request->numero);
        $validaBairro               = HomeController::validaApenasLetras($request->bairro);

        $erro = '';
        if(!$validaNome)                $erro .= 'O nome deve ter apenas letras e espaços. ';
        if(!$validaCPF)                 $erro .= 'CPF inválido. ';
        if($verificaCPFRepetido)        $erro .= 'Esse cpf já está registrado. ';
        if($verificaMatriculaRepetida)  $erro .= 'Essa matrícula já foi inserida. ';
        if(!$validaRua)                 $erro .= 'A rua deve ter apenas letras e espaços. ';
        if(!$validaNumero)              $erro .= 'O número deve ter somente caracteres numéricos. ';
        if(!$validaBairro)              $erro .= 'O bairro deve ter apenas letras e espaços. ';

        if(!$validaNome || !$validaCPF || $verificaCPFRepetido || $verificaMatriculaRepetida || !$validaRua || !$validaNumero || !$validaBairro) {
            return Response::json(array(
                'success' => false,
                'msgErro' => $erro
            )); 
        }

        $user               = Aluno::where('id', $request->usuario_id)->first();
        $user->nome         = $request->nome;
        $user->matricula    = $request->matricula;
        $user->cpf          = $request->cpf;
        HomeController::editaEndereco($request->endereco_id, $request->rua, $request->numero, $request->bairro);
        $user->save();
        return Response::json(array(
            'success' => true
        )); 
    }

    public function editaEndereco($id, $rua, $numero, $bairro) {
        $endereco = Endereco::where('id', $id)->first();
        $endereco->logradouro = $rua;
        $endereco->numero = $numero;
        $endereco->bairro = $bairro;
        $endereco->save();
    }

    public function excluiAluno(Request $request) 
    {
        // dd($request->usuario_id);
        $aluno = Aluno::where('id', $request->usuario_id)->first();
        $alunoExcluido = $aluno->delete();
        if($alunoExcluido) {
            return Response::json(array(
                'success' => true
            )); 
        } else {
            return Response::json(array(
                'success' => false
            )); 
        }
    }

    public function rankAluno() {
        $todosAlunos = Aluno::all();
        $totalAlunos = $todosAlunos->count();

        $somaMedia = 0;
        $idAlunoMaiorMedia = 0;
        $idAlunoMenorMedia = 0;
        $melhorMedia = 0;
        $piorMedia = 0;
        $grupoAlunos = array();
        foreach ($todosAlunos as $aluno) {
            $somaMedia =+ Nota::where('aluno_id', $aluno->id)->avg('valor');
            if($melhorMedia < Nota::where('aluno_id', $aluno->id)->avg('valor') && !is_null(Nota::where('aluno_id', $aluno->id)->avg('valor'))) {
                $melhorMedia = Nota::where('aluno_id', $aluno->id)->avg('valor');
                $idAlunoMaiorMedia = $aluno->id;
            }
            if(($piorMedia > Nota::where('aluno_id', $aluno->id)->avg('valor') || $piorMedia == 0) && !is_null(Nota::where('aluno_id', $aluno->id)->avg('valor'))) {
                $piorMedia = Nota::where('aluno_id', $aluno->id)->avg('valor');
                $idAlunoMenorMedia = $aluno->id;
            }
            $arrayAlunoNomeNota = array("nome" => $todosAlunos->where('id', $aluno->id)->first()->nome, "nota" => Nota::where('aluno_id', $aluno->id)->avg('valor'));
            array_push($grupoAlunos, $arrayAlunoNomeNota );
        }
        if($todosAlunos->where('id', $idAlunoMaiorMedia)->first()) $melhorAluno = $todosAlunos->where('id', $idAlunoMaiorMedia)->first()->nome;
        else $melhorAluno = 'Sem registro';
        if($todosAlunos->where('id', $idAlunoMenorMedia)->first()) $piorAluno   = $todosAlunos->where('id', $idAlunoMenorMedia)->first()->nome;
        else $piorAluno = 'Sem registro';
        if($totalAlunos != 0) $mediaTurma = $somaMedia / $totalAlunos;
        else $mediaTurma = 'Sem alunos registrados para calcular a média.';

        return Response::json(array(
            'success'       => true,
            'mediaTurma'    => $mediaTurma,
            'melhorAluno'   => $melhorAluno,
            'piorAluno'     => $piorAluno,
            'grupoAlunos'   => $grupoAlunos
        )); 
    }

    public function validaApenasLetras($valor) {
        return ctype_alpha(str_replace(' ', '', $valor));
    }

    public function validaCpf($cpf) {

        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
     
        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;

    }

    public function verificaCampoRepetido($campo, $valor) {
        if (Aluno::where($campo, '=', $valor)->exists()) return true;
        return false;
    }

    public function verificaEdicaoCampoRepetido($campo, $valor, $id) {
        if (Aluno::where($campo, '=', $valor)->where('id', '!=', $id)->exists()) return true;
        return false;
    }

    public function validaNota($valor) {
        return is_numeric($valor) && $valor >= 0 && $valor <= 10;
    }

    public function validaApenasNumero($valor) {
        return is_numeric($valor);
    }
}